import React, { useEffect, useState } from "react";
import ExternalSetHeight from "../hoc";
import "./styles.css";

const ExternalSetHeight = (WrappedComponent) => {
  const setDivHeight = window.setDivHeight;
  const setHeight = (e, cb) => {
    setDivHeight(e.target.value);
    if (cb) cb();
  };

  return () => (
    <WrappedComponent setHeight={setHeight} />
  );
};

export default ExternalSetHeight;


const SizingApp = ({ setHeight }) => {
  const [size, setSize] = useState(0);
  const [divHeight, setDivHeight] = useState(window.divHeight);

  useEffect(() => {
    if (window) {
      setWindowSize();
      window.addEventListener("resize", setWindowSize);
    }

    return () => {
      if (window) window.removeEventListener("resize", setWindowSize);
    };
  }, []);

  const setWindowSize = () => {
    setSize(window.innerWidth);
  };

  const setHeightAfterChange = () => {
    setDivHeight(window.divHeight);
  };

  return (
    <div className="sizing-app" style={{ height: `${divHeight}px` }}>
      <span>{size}</span>
      <input
        name="height"
        placeholder="div height"
        value={divHeight || 0}
        onChange={(e) => setHeight(e, setHeightAfterChange)}
      />
    </div>
  );
};

export default ExternalSetHeight(SizingApp);

// css:
// .sizing-app {
//   border: 1px solid black;
// }